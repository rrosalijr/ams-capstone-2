@extends('layouts.app')

@section('content')
	<div class="container">
		<div class="row">
			<div class="col-12">
				<h1 class="text-center">
					Edit Item
				</h1>
			</div>
		</div>

		{{-- edit div start --}}
		<div class="row">
			<div class="col-12 col-md-6 mx-auto">
				<form 
					action="{{route('items.update', $item->id)}}"
					method="post" 
				>
					@csrf
					@method('PUT')
					<label for="name">Category Name:</label>
					<input type="text" name="name" id="name" class="form-control form-control-sm" value="{{$item->name}}">
					<button class="btn btn-sm btn-warning mt-1">Edit</button>
				</form>
			</div>
		</div>
		{{-- edit div end --}}
	</div>
@endsection