@extends('layouts.app')

@section('content')
	<div class="container">
		<div class="row">
			<div class="col-12">
				<h1 class="text-center">
					Items
				</h1>
			</div>
		</div>
		
		{{-- alert message --}}
		@includeWhen(Session::has('message'),'partials.alert')
		@if($errors->any())
			<div class="alert alert-danger">
				<ul>
					@foreach($errors->all() as $error)
					<li>{{$error}}</li>
					@endforeach
				</ul>
			</div>
		@endif
		{{-- end alert message --}}

		{{-- start item list --}}
		<div class="row">	
			{{-- sidebar start --}}
			<div class="col-4 col-lg-3">
				<div class="list-group sticky-top py-2" id="list-tab" role="tablist">
					<a class="list-group-item list-group-item-action active" id="list-home-list" data-toggle="list" href="#list-home" role="tab" >All Items</a>
					@can('isAdmin')
						<a class="list-group-item list-group-item-action" id="list-profile-list" data-toggle="list" href="#list-profile" role="tab" >Create Item</a>
					@endcan
				</div>
			</div>
			{{-- sidebar end --}}

			{{-- content page start --}}
			<div class="col-8 col-lg-9">
				<div class="tab-content row" id="nav-tabContent">
					<div class="tab-pane fade col-8 col-md-12 mx-auto active show" id="list-home" role="tabpanel">
							@foreach($items as $item)
								<?php $count = collect($assets)->where('item_id', $item->id)->where('product_status_id', 1)->count();
									$count_total = collect($assets)->where('item_id', $item->id)->count();
								 ?>
								@include('items.partials.card')
							@endforeach
					</div>

					{{-- start form --}}
					<div class="tab-pane fade col-8 col-md-12 mx-auto" id="list-profile" role="tabpanel">
						<form action="{{route('items.store')}}" method="post">
							@csrf
							<label for="name">Item Name:</label>
							<input type="text" name="name" id="name" class="form-control form-control-sm">
							<button class="btn btn-sm btn-primary mt-2">Create Item</button>
						</form>
					</div>
				</div>
			</div>
			{{-- content page end --}}
		</div>
		{{-- end item list --}}
	</div>
@endsection