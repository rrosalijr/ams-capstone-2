<div class="card my-1">
	<div class="card-body">
		<h5 class="card-title mb-0 w-100 align-items-center justify-content-between">
			{{$item->name}}
		</h5>

		@include('items.partials.card-body')
	</div>
</div>