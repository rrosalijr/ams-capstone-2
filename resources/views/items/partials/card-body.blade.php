<p class="card-text">
	Current Available: {{$count}}/{{$count_total}}
</p>

<div>
	<div class="d-inline">

		@if(!isset($view))
		{{-- View button --}}
		<a href="{{route('items.show', $item->id)}}" class="btn btn-primary">
			View
		</a>
		@endif
		
		@can('isAdmin')
			{{-- edit button --}}
			<a href="{{route('items.edit', $item->id)}}" class="btn btn-warning">
				Edit
			</a>

			{{-- delete form --}}
			<form action="{{route('items.destroy', $item->id)}}" method="post" class="d-inline-block">
				@csrf
				@method('DELETE')
				<button class="btn btn-danger">Delete</button>
			</form>
		@endcan
	</div>
</div>