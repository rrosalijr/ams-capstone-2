@extends('layouts.app')

@section('content')
	<main class="py-4">
		<div class="container">
			<div class="row">
				<div class="col-12 col-md-4 col-lg-3 mx-auto">
					<h1>{{$item->name}}</h1>
					@include('items.partials.card-body',['view' => false])
					<hr>
					@can('isAdmin')
						<div class="my-5">
							<h5 class="text-center">Create Asset</h5>
							<form action="{{route('assets.store')}}" method="post" enctype="multipart/form-data">
								@csrf
								<?php $asset_code = strtoupper(Str::random(6))?>
									<label for="name">Asset Name:</label>
									<input type="text" name="name" id="name" class="form-control form-control @error('name') is-invalid @enderror">
									@error('name')
										<small class="d-block invalid-feedback">
											<strong>
												{{ $message }}
											</strong>
										</small>
									@enderror
									<small id="assetName" class="text-muted">Name of the product to be uploaded.</small>
									<label for="asset_code">Control Code:</label>
									
									<div class="input-group mb-3">
										<div class="input-group-prepend">
											<span class="input-group-text" id="basic-addon1">{{$item->id}}-</span>
										</div>
										<input 
											type="text" 
											id="asset_code" 
											name="asset_code" 
											aria-label="Asset Code" 
											aria-describedby="basic-addon1" 
											placeholder="Asset Code" 
											class="form-control" 
											value="{{$asset_code}}"
										>
									</div>

									<small id="controlCode" class="text-muted">Control Code should be unique to each unit.</small>
								<div class="form-group">
									<label for="item_id">Item Model</label>
									<select type="text" name="item_id" id="item_id" class="form-control" placeholder="Item Model" aria-describedby="ItemId" value="{{$item->id}}" disabled>
										<option value="{{$item->id}}" selected="">{{$item->name}}</option>
									</select>
									<small id="ItemId" class="text-muted">Model of the unit</small>
								</div>
								<input type="hidden" name="item_id" value="{{$item->id}}">
								<div class="form-group">
									<label for="image">Image:</label>
									<input type="file" name="image" id="image" class="form-control-file @error('image') is-invalid @enderror" aria-describedby="image" placeholder="Image">
									@error('image')
										<small class="d-block invalid-feedback">
											<strong>
												{{ $message }}
											</strong>
										</small>
									@enderror
									<small id="image" class="form-text text-muted">Item Sample Image</small>
								</div>
								<button type="submit" class="btn btn-primary w-100">Create Unit</button>
							</form>
						</div>
					@endcan
				</div>
				{{-- card list start --}}
				<div class="col-12 col-md-8 col-lg-9">
					<div class="row">
						@foreach($assets as $asset)
							<div class="col-12 col-sm-6 col-lg-4 my-3">	
								{{-- card start --}}
								<div class="card my-2">
									<img src="{{asset($asset->image)}}" alt="" class="card-img-top">
									<div class="card-body">
										<p class="card-text">
											<strong>{{$asset->name}}</strong>
										</p>
										<p class="card-text">
											Control Code: <strong>{{$asset->item_id}}-{{$asset->asset_code}}</strong>
										</p>
										<p class="cart-text">
											Status: <span class="badge badge-{{$asset->productStatus->id === 1 ? 'success' : 'danger'}}">{{$asset->productStatus->name}}</span>
										</p>

										{{-- Request Button --}}
										@include('assets.partials.request')
										
										@can('isAdmin')
											{{-- Edit Button --}}
											@include('assets.partials.edit')

											{{-- Delete Button --}}
											@include('assets.partials.delete')
										@endcan
									</div>
								</div>
							</div>
							{{-- card list end --}}
							@include('assets.partials.edit-modal')
						@endforeach
					</div>
				</div>
		</div>
	</main>
@endsection