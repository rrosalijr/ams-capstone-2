@if(Auth::user())
	@extends('layouts.app')
	@section('content')
		
		<div class="container">
			<div class="row">
				<div class="col-12">
					<h4 class="text-center">
						Asset Request Form
					</h4>
				</div>
			</div>
			
			@if(!Session::has('request'))
				{{-- alert start --}}
				<div class="alert alert-info alert-dismissible fade show text-center" role="alert">
					<button type="button" class="close" data-dismiss="alert" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
					<strong>Request Form is empty!</strong> 
				</div>
				{{-- alert end --}}
			@else	
				<div class="row">
					<div class="col-12">
						{{-- start cart table --}}
						<table class="table table-hover text-center">
							<thead>
								<tr>
									<th>Asset Name</th>
									<th>Asset Code</th>
									<th>Current Status</th>
									<th>Action</th>
								</tr>
							</thead>
							<tbody>
								{{-- start item row --}}
								@foreach($assets as $asset)
								<tr>
									<td scope="row">{{$asset->name}}</td>
									<td><strong>{{$asset->id}}-{{$asset->asset_code}}</strong></td>
									<td><span class="badge badge-success">{{$asset->productStatus->name}}</span></td>
									<td>
										<form action="{{route('request.destroy', $asset->id)}}" method="post">
											@csrf
											@method('DELETE')
											<button class="btn btn-outline-danger w-100">Remove</button>
										</form>
									</td>
								</tr>
								@endforeach
								{{-- end item row --}}
							</tbody>
						</table>
						{{-- end cart table --}}
					</div>
				</div>
				<div class="row">
					<div class="col-12 col-sm-8 mx-auto">
						@guest
							<a href="{{route('login')}}" class="btn btn-sm btn-success w-100 my-1">Login to Submit Request</a>
							@else
								<form action="{{route('tickets.store')}}" method="post">
									@csrf
									<div class="row align-items-center">
										<div class="col align-self-start">
											<div class="form-group">
												<label for="date_needed_from">Date Needed</label>
												<input type="date" min="<?= date('Y-m-d',strtotime("+1 day")); ?>" max="<?= date('Y-m-d',strtotime("+30 day"))?>" name="date_needed_from" id="date_needed_from" class="form-control @error('date_needed_from') is-invalid @enderror" placeholder="Date Needed" aria-describedby="dataNeededFrom" value="<?= date('Y-m-d',strtotime("+1 day")); ?>">
												@error('date_needed_from')
													<small class="d-block invalid-feedback">
														<strong>
															{{ $message }}
														</strong>
													</small>
												@enderror
												<small id="dateNeededFrom" class="text-muted">Date when you need to have this asset</small>
											</div>
										</div>
										<div class="col align-self-start">
											<div class="form-group">
												<label for="date_needed_to">Date to return</label>
												<input type="date" min="<?= date('Y-m-d',strtotime("+3 day")); ?>" max="<?= date('Y-m-d',strtotime("+30 day"))?>" name="date_needed_to" id="date_needed_to" class="form-control @error('date_needed_to') is-invalid @enderror" placeholder="Date Needed" aria-describedby="dateNeededTo" value="<?= date('Y-m-d',strtotime("+8 day")); ?>">
												@error('date_needed_to')
													<small class="d-block invalid-feedback">
														<strong>
															{{ $message }}
														</strong>
													</small>
												@enderror
												<small id="dateNeededTo" class="text-muted">Date when you will return these assets</small>
											</div>
										</div>
									</div>
									<button class="btn btn-sm btn-primary">Submit Request</button>
								</form>
						@endguest
					</div>
				</div>
				<hr>

				<div class="row">
					<div class="col-12">
						<form action="{{route('request.clear')}}" method="post">
							@csrf
							@method('DELETE')
							<button class="btn btn-sm btn-outline-danger">Clear Request Form</button>
						</form>
					</div>
				</div>
			@endif

		</div>
		
	@endsection
@endif