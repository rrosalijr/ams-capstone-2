@extends('layouts.app')

@section('content')
	<div class="container container-fluid">

		{{-- header start --}}
		<div class="row">
			<div class="col-12">
				<h1 class="text-center">
					Assets
				</h1>
			</div>
		</div>
		{{-- header end --}}

		{{-- assets section start --}}
		<div class="row">
			@foreach($assets as $asset)
				<div class="col-12 col-sm-6 col-md-4 col-lg-3 mb-2">
					{{-- card start --}}
					<div class="card my-2">
						<img src="{{$asset->image}}" alt="" class="card-img-top">
						<div class="card-body">
							<p class="card-text">
								<strong>{{$asset->name}}</strong>
							</p>
							<p class="card-text">
								Control Code: <strong>{{$asset->asset_code}}</strong>
							</p>
							<p class="card-text">
								Status: <span class="badge badge-{{$asset->productStatus->id === 1 ? 'success' : 'danger'}}">{{$asset->productStatus->name}}</span>
							</p>

							{{-- Request Button --}}
							@include('assets.partials.request')
							
							@can('isAdmin')
								{{-- Edit Button --}}
								@include('assets.partials.edit')

								{{-- Delete Button --}}
								@include('assets.partials.delete')
							@endcan
						</div>
					</div>
					{{-- card list end --}}
				</div>
			@endforeach
		</div>
		{{-- assets section end --}}

	</div>
@endsection