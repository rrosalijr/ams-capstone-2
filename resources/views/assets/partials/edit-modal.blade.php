<!-- Modal -->
<div class="modal fade" id="editModal{{$asset->id}}" tabindex="-1" role="dialog" aria-labelledby="editModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="editModalLabel">Edit {{$asset->asset_code}}</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col">
            <img src="{{asset($asset->image)}}" alt="" id="modalImg" class="w-100 max-h-50">
          </div>
          <div class="col">
            <div class="form-group">
              <label for="asset_code">Control Code:</label>
              <input 
                type="text" 
                name="asset_code" 
                id="asset_code_modal" 
                class="form-control" 
                placeholder="Asset Code" 
                aria-describedby="assetCode"
                value="{{$asset->asset_code}}"
                disabled 
              >
              <small id="assetCode" class="text-muted">Control Code</small>
            </div>
            <form action="{{route('assets.update', $asset->id)}}" id="edit-units-modal-{{$asset->id}}" method="post" enctype="multipart/form-data">
              @csrf
              @method('PUT')
              <div class="form-group">
                <label for="name">Asset Name:</label>
                <input type="text" name="name" id="name" class="form-control form-control-sm" aria-describedby="assetName" value="{{$asset->name}}">
                <small id="assetName" class="text-muted">Asset Name</small>
              </div>
              <div class="form-group">
                <label for="image">Set New Image</label>
                <input type="file" name="image" id="modal-form-image" class="form-control-file" placeholder="Image" aria-describedby="image">
              </div>
              <div class="form-group">
                <label for="product_status_id">Status</label>
                <select name="product_status_id" id="product_status_id" class="form-control" placeholder="status" aria-describedby="product_status_id">
                  @foreach($product_statuses as $product_status)
                    <option 
                    value="{{$product_status->id}}"
                    {{ $product_status->id === $asset->product_status_id ? "selected" : ""}}
                    >
                    {{$product_status->name}}</option>
                  @endforeach
                </select>
                <small id="statusId" class="text-muted">Edit Status</small>
              </div>
            </form>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" id="submit-edit-units-modal" form="edit-units-modal-{{$asset->id}}" class="btn btn-primary">Save changes</button>
      </div>
    </div>
  </div>
</div>