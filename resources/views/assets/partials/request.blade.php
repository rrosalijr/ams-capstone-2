<form action="{{route('request.update',$asset->id)}}" method="post" class="my-2">
	@csrf
	@method('PUT')
	<button class="btn btn-sm btn-success w-100 mt-1" {{$asset->product_status_id !== 1 ? 'disabled' : '' }}>
		Request
	</button>
</form>