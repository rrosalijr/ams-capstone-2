<form action="{{route('assets.destroy', $asset->id)}}" method="post">
	@csrf
	@method('DELETE')
	<button class="btn btn-sm btn-danger w-100 mt-1">Delete</button>
</form>