{{-- table start --}}
<div class="table-responsive">
  <table class="table table-hover">
    {{-- ticket code --}}
    <tr>
      <td>Ticket Code</td>
      <td>{{$ticket->ticket_code}}</td>
    </tr>
    {{-- ticket code end --}}
    
    {{-- Customer name start --}}
    <tr>
      <td>Customer</td>
      <td>{{$ticket->user->name}}</td>
    </tr>
    {{-- Customer name end --}}

    {{-- Date start --}}
    <tr>
      <td>Date</td>
      <td>{{date('M d, Y',strtotime($ticket->created_at))}}</td>
    </tr>
    {{-- Date end --}}

    {{-- Status start --}}
    <tr>
      <td>Status</td>
      <td>
        <span class="badge badge-<?php
                    if($ticket->ticket_status_id === 1)
                    {
                      echo "warning";
                    } elseif($ticket->ticket_status_id === 2)
                    {
                      echo "info";
                    } elseif($ticket->ticket_status_id === 3)
                    {
                      echo "success";
                    } elseif($ticket->ticket_status_id === 4)
                    {
                      echo "danger";
                    }
                ?>
              ">
                {{$ticket->ticketStatus->name}}
          </span>
        @can('isAdmin')
          @include('tickets.partials.edit-status')
        @endcan
      </td>
    </tr>
    {{-- Status end --}}

    {{-- Date needed start --}}
    <tr>
      <td>Date needed</td>
      <td>{{date('M d, Y',strtotime($ticket->date_needed_from))}}</td>      
    </tr>
    {{-- Date needed end--}}
    
    {{-- Date return start --}}
    <tr>
      <td>Return Date</td>
      <td>{{date('M d, Y',strtotime($ticket->date_needed_to))}}</td>
    </tr>
    {{-- Date return end--}}

    {{-- Assets requested start --}}
    <tr>
      <td><strong>Requested Unit:</strong></td>
    </tr>
      @foreach($ticket->assets as $asset)
        <tr>
          <td>{{$asset->asset_code}}</td>
          <td><span class="badge badge-<?php
                    if($asset->product_status_id === 1)
                    {
                      echo "success";
                    } elseif($asset->product_status_id === 2)
                    {
                      echo "danger";
                    }
                    ?>
              ">{{$asset->productStatus->name}}</span></td>
        </tr>
      @endforeach
    {{-- Assets requested end --}}

  </table>
</div>
{{-- table end --}}