<div class="card">
  <div class="card-header" id="heading{{$ticket->id}}">
    <h2 class="mb-0">
      <button class="btn btn-link btn-block text-left" type="button" data-toggle="collapse" data-target="#collapse{{$ticket->id}}" aria-expanded="true" aria-controls="collapse{{$ticket->id}}">
        {{$ticket->ticket_code}}
        <span class="badge badge-<?php
        if($ticket->ticket_status_id === 1)
        {
          echo "warning";
          } elseif($ticket->ticket_status_id === 2)
          {
            echo "info";
            } elseif($ticket->ticket_status_id === 3)
            {
              echo "success";
              } elseif($ticket->ticket_status_id === 4)
              {
                echo "danger";
              }
              ?>
              ">
              {{$ticket->ticketStatus->name}}
        </span>
      </button>
    </h2>
  </div>

  <div id="collapse{{$ticket->id}}" class="collapse" aria-labelledby="heading{{$ticket->id}}" data-parent="#accordionExample">
    <div class="card-body">

      @include('tickets.partials.summary')
        <a href="{{route('tickets.show', $ticket->id)}}">View details</a>
    </div>
  </div>
</div>