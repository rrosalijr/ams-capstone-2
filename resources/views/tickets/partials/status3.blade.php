<div class="accordion" id="accordionExample">

@cannot('isAdmin')
  @foreach($tickets as $ticket)
      @if($ticket->user_id === Auth::user()->id) 
        @if($ticket->ticket_status_id === 3)
          @include('tickets.partials.card')
        @endif
      @endif
  @endforeach
@endcannot

@can('isAdmin')
  @foreach($tickets as $ticket)
    @if($ticket->ticket_status_id === 3)
          @include('tickets.partials.card')
    @endif
  @endforeach
@endcan

  
</div>