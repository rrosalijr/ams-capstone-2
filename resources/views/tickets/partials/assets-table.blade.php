<div class="row">
	<div class="col-12">
		{{-- table start --}}
		<div class="table-responsive">
			<table class="table table-hover">
				<thead>
					<tr>
						<th>Asset Name</th>
						<th>Asset Code</th>
						<th>Date Requested</th>
						<th>Date to be returned</th>
					</tr>
				</thead>
				<tbody>
					{{-- product row start --}}
					@foreach($ticket->assets as $asset)
						<tr>
							<td>{{$asset->name}}</td>
							<td><strong>{{$asset->asset_code}}</strong></td>
							<td>{{date('M d, Y',strtotime($ticket->date_needed_from))}}</td>
							<td>{{date('M d, Y',strtotime($ticket->date_needed_to))}}</td>
						</tr>
					@endforeach
					{{-- product row end --}}
				</tbody>
			</table>
		</div>
		{{-- table end --}}
	</div>
</div>