<div class="accordion" id="accordionAll">

@cannot('isAdmin')
  @foreach($tickets as $ticket)
    {{-- ticket card start --}}
      @if($ticket->user_id === Auth::user()->id)
        <div class="card">
          <div class="card-header" id="heading{{$ticket->id}}">
            <h2 class="mb-0">
              <button class="btn btn-link btn-block text-left" type="button" data-toggle="collapse" data-target="#collapse{{$ticket->id}}" aria-expanded="true" aria-controls="collapse{{$ticket->id}}">
                {{$ticket->ticket_code}}
                <span class="badge 
                  badge-{{$ticket->ticket_status_id === 1? "warning" :
          ($ticket->ticket_status_id === 2 ? "primary" :
          ($ticket->ticket_status_id === 4 ? "danger" : "success"))

        }}">
                 {{$ticket->ticketStatus->name}}
                </span>
              </button>
            </h2>
          </div>
          <div id="collapse{{$ticket->id}}" class="collapse" aria-labelledby="heading{{$ticket->id}}" data-parent="#accordionAll">
            <div class="card-body">
               @include('tickets.partials.summary')
            </div>
          </div>
        </div>
      {{-- ticket card start --}} 
    @endif
  @endforeach
@endcannot

@can('isAdmin')
  @foreach($tickets as $ticket)
    {{-- ticket card start --}}
      <div class="card">
        <div class="card-header" id="heading{{$ticket->id}}">
          <h2 class="mb-0">
            <button class="btn btn-link btn-block text-left" type="button" data-toggle="collapse" data-target="#collapse{{$ticket->id}}" aria-expanded="true" aria-controls="collapse{{$ticket->id}}">
               {{$ticket->ticket_code}}
                <span class="badge badge-{{$ticket->ticket_status_id === 1? "warning" :
                ($ticket->ticket_status_id === 2 ? "primary" :
                ($ticket->ticket_status_id === 4 ? "danger" : "success"))

                }}">
               {{$ticket->ticketStatus->name}}
              </span>
            </button>
          </h2>
        </div>
        <div id="collapse{{$ticket->id}}" class="collapse" aria-labelledby="heading{{$ticket->id}}" data-parent="#accordionAll">
          <div class="card-body">
             @include('tickets.partials.summary')
          </div>
        </div>
      </div>
    {{-- ticket card start --}} 
  @endforeach
@endcan

  
</div>