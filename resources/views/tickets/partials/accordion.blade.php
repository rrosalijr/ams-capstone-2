<div class="row">
  <div class="col-4 col-md-3 mb-3 mb-md-0 mx-auto mx-md-0">
    <div class="list-group" id="list-tab" role="tablist">
      <a class="list-group-item list-group-item-action active" id="all" data-toggle="list" href="#list-all" role="tab" aria-controls="home">
        All
      </a>
      <a class="list-group-item list-group-item-action" id="pending" data-toggle="list" href="#list-profile" role="tab" aria-controls="profile">
        Pending
      </a>
      <a class="list-group-item list-group-item-action" id="approved" data-toggle="list" href="#list-approved" role="tab" aria-controls="messages">
        Approved
      </a>
      <a class="list-group-item list-group-item-action" id="decline" data-toggle="list" href="#list-decline" role="tab" aria-controls="settings">
        Completed
      </a>
      <a class="list-group-item list-group-item-action" id="completed" data-toggle="list" href="#list-completed" role="tab" aria-controls="settings">
        Cancelled
      </a>
    </div>
  </div>
  <div class="col-8 col-md-9  mx-auto mx-md-0">
    <div class="tab-content" id="nav-tabContent">
      <div class="tab-pane fade show active" id="list-all" role="tabpanel" aria-labelledby="list-aa">
        @include('tickets.partials.statusAll')
      </div>
      <div class="tab-pane fade" id="list-profile" role="tabpanel" aria-labelledby="pending">
        @include('tickets.partials.status1')
      </div>
      <div class="tab-pane fade" id="list-approved" role="tabpanel" aria-labelledby="approved">
        @include('tickets.partials.status2')
      </div>
      <div class="tab-pane fade" id="list-decline" role="tabpanel" aria-labelledby="decline">
        @include('tickets.partials.status3')
      </div>
      <div class="tab-pane fade" id="list-completed" role="tabpanel" aria-labelledby="completed">
        @include('tickets.partials.status4')
      </div>
    </div>
  </div>
</div>