@extends('layouts.app')

@section('content')

<div class="container">
	<div class="row">
		<div class="col-12">
			<h1 class="text-center">View Ticket</h1>
		</div>
	</div>

	{{-- ticket section start --}}
	<div class="row">
		<div class="col-12">
			@include('tickets.partials.summary')
		</div>
	</div>
	@include('tickets.partials.assets-table')
	{{-- ticket section end --}}
</div>
@endsection
