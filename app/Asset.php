<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Asset extends Model
{
    protected $fillable = ['name','image','item_id','description','asset_code','product_status_id'];

    public function item()
    {
        return $this->belongsTo('App\Item');
    }

    public function productStatus()
    {
    	return $this->belongsTo('App\ProductStatus');
    }
}
