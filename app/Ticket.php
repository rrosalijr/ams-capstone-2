<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ticket extends Model
{
	protected $fillable = ['ticket_status_id'];

	public function ticketStatus()
	{
		return $this->belongsTo('App\TicketStatus');
	}

	public function user()
	{
		return $this->belongsTo('App\User');
	}

	public function assets()
    {
    	return $this->belongsToMany('App\Asset');
    }
}
