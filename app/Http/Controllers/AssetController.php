<?php

namespace App\Http\Controllers;

use App\Asset;
use App\Item;
use App\ProductStatus;
use Illuminate\Http\Request;
use Storage;

class AssetController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->authorize('viewAny', Asset::class);
        
        $assets = Asset::all();

        return view('assets.index')
            ->with('assets', $assets);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->authorize('create', Asset::class);

        return view('assets.create')
            ->with('items', Item::all())
            ->with('product_statuses', ProductStatus::all());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->authorize('create', Asset::class);

        $validatedData = $request->validate([
            'name' => 'required|string',
            'image' => 'required|image|max:2000',
            'asset_code' => 'required',
            'item_id' => 'required'
        ]);

        $asset = new Asset($validatedData);

        $path = $request->file('image');
        $path_name = time().".".$path->getClientOriginalExtension();
        $destination = "image/";
        $path->move($destination, $path_name);
        $asset->image = $destination.$path_name;

        $asset->save();

        return back()
            ->with('message',"Asset added successfully.");
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Asset  $asset
     * @return \Illuminate\Http\Response
     */
    public function show(Asset $asset)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Asset  $asset
     * @return \Illuminate\Http\Response
     */
    public function edit(Asset $asset)
    {
        $this->authorize('update', $asset);

        return view('aseets.show')
            ->with('asset',$asset);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Asset  $asset
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Asset $asset)
    {
        $this->authorize('update', $asset);

        $validatedData = $request->validate([
            'name' => 'required|string',
            'image' => 'image|max:2000',
            'product_status_id' => 'required'
        ]);

        $asset->product_status_id = $request->product_status_id;

        $asset->update($validatedData);

        $path = $request->file('image');

        if($path != ""){
            $path_name = time().".".$path->getClientOriginalExtension();
            $destination = "image/";
            $path->move($destination, $path_name);
            $asset->image = $destination.$path_name;
        }

        $asset->save();

        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Asset  $asset
     * @return \Illuminate\Http\Response
     */
    public function destroy(Asset $asset)
    {
        $this->authorize('delete', $asset);

        $asset->delete();
        return redirect(route('assets.index'));
    }
}
