<?php

namespace App\Http\Controllers;

use App\Ticket;
use App\Asset;
use App\TicketStatus;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Str;
use Auth;

class TicketController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->authorize('viewAny', Ticket::class);

        $tickets = Ticket::all();

        return view('tickets.index')
            ->with('tickets', $tickets)
            ->with('ticket_statuses', TicketStatus::all())
            ->with('assets', Asset::all());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->authorize('create', Ticket::class);
        $ticket = new Ticket;
        $ticket->ticket_code = strtoupper(Str::random(6));
        $ticket->user_id = Auth::user()->id;
        $ticket->date_needed_from = $request->date_needed_from;
        $ticket->date_needed_to = $request->date_needed_to;

        $ticket->save();

        $assets = Asset::find(array_keys(session('request')));

        foreach ($assets as $asset)
        {
            $ticket->assets()->attach($asset->id);
        }


        session()->forget('request');

        return redirect(route('tickets.show',$ticket->id));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Ticket  $ticket
     * @return \Illuminate\Http\Response
     */
    public function show(Ticket $ticket)
    {
        $this->authorize('view', $ticket);
        return view('tickets.show')
            ->with('ticket', $ticket)
            ->with('ticket_statuses', TicketStatus::all())
            ->with('assets', Asset::all());
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Ticket  $ticket
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, Ticket $ticket)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Ticket  $ticket
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Ticket $ticket)
    {
        $this->authorize('update', $ticket);
        $validatedData = $request->validate([
            "ticket_status_id" => 'required'
        ]);
        $ticket->update($validatedData);
        $ticket->save();

        $data = DB::table('asset_ticket')->get();

        foreach ($data as $asset_ticket)
        {
            $ticket_id = $asset_ticket->ticket_id;
            $asset_id = $asset_ticket->asset_id;

            $items = DB::table('tickets')->where('id', $ticket_id)->get();

            foreach ($items as $item)
            {
                if ($item->ticket_status_id === 2)
                {
                    $assets = DB::table('assets')
                        ->where('id', $asset_id)->get();
                    foreach ($assets as $asset)
                    {
                        $asset = db::table('assets')
                            ->where('id', $asset_id)
                            ->update(['product_status_id' => 2]);
                    }
                }
            }

            if ($item->ticket_status_id !== 2)
            {
                $assets = DB::table('assets')
                    ->where('id', $asset_id)->get();
                foreach ($assets as $asset)
                {
                    $asset = DB::table('assets')
                        ->where('id', $asset_id)
                        ->update(['product_status_id' => 1]);
                }
            }
        }

        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Ticket  $ticket
     * @return \Illuminate\Http\Response
     */
    public function destroy(Ticket $ticket)
    {
        //
    }
}
