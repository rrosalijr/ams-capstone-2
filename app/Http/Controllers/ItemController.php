<?php

namespace App\Http\Controllers;

use App\Item;
use App\Asset;
use App\ProductStatus;
use Illuminate\Http\Request;
use Str;

class ItemController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->authorize('viewAny', Item::class);

        $items = Item::all();
        return view('items.index')->with('items', $items)
            ->with('assets', Asset::all());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->authorize('create', Item::class);

        return view('items.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->authorize('create', Item::class);

        $validatedData = $request->validate([
            'name' => 'required|unique:items,name'
        ]);
        $item = new Item($validatedData);
        $item->save();
        return redirect(route('items.index'))
            ->with('message', "Item is added successfully.");
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Item  $item
     * @return \Illuminate\Http\Response
     */
    public function show(Item $item)
    {
        $this->authorize('view', $item);

        $assets = Asset::all();
        $count = collect($assets)->where('item_id', $item->id)->where('product_status_id', 1)->count();
        $count_total = collect($assets)->where('item_id', $item->id)->count();
        return view('items.show')->with('item',$item)
            ->with('assets', $assets->where('item_id',$item->id))
            ->with('product_statuses', ProductStatus::all())
            ->with('count', $count)
            ->with('count_total', $count_total);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Item  $item
     * @return \Illuminate\Http\Response
     */
    public function edit(Item $item)
    {
        $this->authorize('update', $item);

        return view('items.edit')->with('item',$item);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Item  $item
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Item $item)
    {
        $this->authorize('update', $item);

        $item->name = $request->name;
        $item->save();
        return redirect(route('items.show', $item->id))
            ->with('message', "Item is updated successfully.");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Item  $item
     * @return \Illuminate\Http\Response
     */
    public function destroy(Item $item)
    {
        $this->authorize('delete', $item);

        $assets = Asset::all();
        $count_total = collect($assets)->where('item_id',$item->id)->count();
        if($count_total === 0)
        {
            $item->delete();
            return redirect(route('items.index'))
                ->with('message', "Item has been deleted successfully.")
                ->with('alert','warning');
        }
        return back()
            ->with('message', 'Cannot delete Item with available asset.');
    }
}
