<?php

use Illuminate\Database\Seeder;

class ItemSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('items')->insert([
        	'name' => 'Electronics'
        ]);

        DB::table('items')->insert([
        	'name' => 'Stationary'
        ]);

        DB::table('items')->insert([
        	'name' => 'Furniture'
        ]);
    }
}
