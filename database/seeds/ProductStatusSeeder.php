<?php

use Illuminate\Database\Seeder;

class ProductStatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('product_statuses')->insert([
        	'name' => 'available'
        ]);

        DB::table('product_statuses')->insert([
        	'name' => 'not available'
        ]);
    }
}
