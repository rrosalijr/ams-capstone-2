<?php

use Illuminate\Database\Seeder;

class TicketStatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('ticket_statuses')->insert([
        	'name' => 'pending'
        ]);

        DB::table('ticket_statuses')->insert([
        	'name' => 'approved'
        ]);

        DB::table('ticket_statuses')->insert([
        	'name' => 'completed'
        ]);

        DB::table('ticket_statuses')->insert([
        	'name' => 'cancelled'
        ]);
    }
}
