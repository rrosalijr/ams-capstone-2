<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
        	RoleSeeder::class,
        	ItemSeeder::class,
            ProductStatusSeeder::class,
            TicketStatusSeeder::class,
        	UserSeeder::class
        ]);
    }
}
